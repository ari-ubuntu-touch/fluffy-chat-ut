#!/bin/bash


echo "Setting up flutter..."

remove_old () {
    rm -rf flutter
}

flutter_clone_and_compile () {
    git clone --recurse-submodules https://gitlab.com/famedly/fluffychat
    cd fluffychat
    ./scripts/prepare-web.sh # To install libolm
    flutter build web --release
    cd ..
}

move_and_delete () {
    mv fluffychat/build/web/ flutter
    rm -rf fluffychat
}

apply_post_patches () {
    # Change base directory
    sed -i 's/<base href="\/web\/">/<base href="\/assets\/flutter\/">/g' flutter/index.html

    # Fix a bug which prevents loading
    sed -i 's/reg.installing ?? reg.waiting/reg.installing ? reg.waiting : reg.intsalling/g' flutter/index.html

    # Fix loading screen icon position
    sed -i 's/class="center"/style="position: absolute; left: 25vw;"/g' flutter/index.html
}


remove_old
flutter_clone_and_compile
move_and_delete
apply_post_patches


echo "Done."


