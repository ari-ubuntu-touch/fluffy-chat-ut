# Fluffy Chat UT

Fluffy chat wrapper for Ubuntu Touch.

This repo is in no way associated with [!Fluffy Chat](https://gitlab.com/famedly/fluffychat).
All credit goes to them for the great app.


## Build Instructions

Run the prebuild script.

```bash
./setupflutter.sh
```

This script will pull the Fluffy Chat flutter source code and compile it for web.

The you can use the following to install it to your device

```bash
clickable build --skip-review && clickable install --ssh YOUR_DEVICE_IP_HERE
```

[![OpenStore](https://open-store.io/badges/en_US.svg)](https://open-store.io/app/fluffychatflutter.red)

## License

Copyright (C) 2022  Red

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
