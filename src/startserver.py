'''
 Copyright (C) 2022  Red

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3.

 fluffychatflutter is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from http.server import HTTPServer, CGIHTTPRequestHandler

port = 8070

def start():
    print("Starting the fluffy server...")
    # Create server object listening the port specified
    server_object = HTTPServer(server_address=('', port), RequestHandlerClass=CGIHTTPRequestHandler)
    print("I've created and now serving...")
    # # Start the web server
    server_object.serve_forever()
    print("Finished serving....")
