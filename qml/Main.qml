/*
 * Copyright (C) 2022  Red
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * fluffychatflutter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Window 2.12
import Qt.labs.settings 1.0
import Morph.Web 0.1
import QtWebEngine 1.10
import Ubuntu.PushNotifications 0.1
import io.thp.pyotherside 1.4
import QtQuick.Controls 2.1
import Ubuntu.Components.Popups 1.3
import Ubuntu.Content 1.3
import Ubuntu.DownloadManager 1.2


MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'fluffychatflutter.red'



    /* Don't block content with the keyboard  */
    automaticOrientation: true
    anchors {
        fill : parent
        bottomMargin : UbuntuApplication.inputMethod.visible
            ? UbuntuApplication
                .inputMethod
                .keyboardRectangle
                .height / Screen.devicePixelRatio
            : 0
        Behavior on bottomMargin {
            NumberAnimation {
                duration : 175
                easing.type : Easing.OutQuad
            }
        }
    }



    width: units.gu(45)
    height: units.gu(75)

    /* PushClient { */
    /*     id: pushClient */
    /*     appId: "fluffychatflutter.red_fluffychatflutter" */
    /*     onTokenChanged: console.log("👍", pushClient.token) */
    /* } */

    Timer {
        id: timer
    }

    function delay(delayTime, cb) {
        timer.interval = delayTime;
        timer.repeat = false;
        timer.triggered.connect(cb);
        timer.start();
    }


    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../src/'));

            importModule('startserver', function() {
                console.log('module imported');
                python.call('startserver.start', [], function(returnValue) {
                    console.log('server started.');
                })
            });
        }

        onError: {
            console.log('python error: ' + traceback);
        }
    }

    Page {
        anchors.fill: parent

        header: WebEngineView {
            id: webview
            anchors { fill: parent }
            //focus: true
            zoomFactor: units.gu(1) / 8.4
            settings.pluginsEnabled: true
            url: "about:blank"

            profile:  WebEngineProfile {
                id: webContext
	              httpUserAgent: "Mozilla/5.0 (Linux; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.5195.58 Mobile Safari/537.36"
                storageName: "Storage"
                persistentStoragePath: "/home/phablet/.cache/fluffychatflutter.red/QtWebEngine"
            }


            /* Need to wait until the python server has started
            before opening the url*/
            /* TODO: Have python run this as soon as the server has started */
            /* Instead of waiting an arbitrary amount of time */
            Component.onCompleted: {
                print("I'm printed right away..")
                delay(2000, function() {
                    print("And I'm printed after 1 second!")
                    webview.url = "http://localhost:8070/assets/flutter/"
                })
            }

            onFileDialogRequested: function(request) {
                request.accepted = true;
                var importPage = mainPageStack.push(Qt.resolvedUrl("ImportPage.qml"),{"contentType": ContentType.All, "handler": ContentHandler.Source})
                importPage.imported.connect(function(fileUrl) {
                    console.log(String(fileUrl).replace("file://", ""));
                    request.dialogAccept(String(fileUrl).replace("file://", ""));
                    mainPageStack.push(pageMain)
                })
            }
            onNewViewRequested: {
                request.action = WebEngineNavigationRequest.IgnoreRequest
                if(request.userInitiated) {
                    Qt.openUrlExternally(request. requestedUrl)
                }
            }
            onFeaturePermissionRequested: function(url, feature) {
                print(url, feature)
                grantFeaturePermission(url, feature, true)
            }
        }
    }
}
